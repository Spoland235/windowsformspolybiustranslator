﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SimpleEncryption
{
    public partial class Form1 : Form
    {
        string fileToEncrypt = "";

        string saveAs = "";

        string[] translatedLines;

        readonly char[] alphabet = { 'A', 'B', 'C', 'D', 'E',
                                        'F', 'G', 'H', 'I', 'K',
                                        'L', 'M', 'N', 'O', 'P',
                                        'Q', 'R', 'S', 'T', 'U',
                                        'V', 'W', 'X', 'Y', 'Z' };

        char GetLetter(int y, int x)
        {
            y -= 1;
            x -= 1;

            Console.WriteLine(y + " " + x);

            return alphabet[y * 5 + x];
        }

        public Form1()
        {
            InitializeComponent();

            encryptFileDialog.Disposed += EncryptFileDialog_Disposed;

        }

        private void EncryptFileDialog_Disposed(object sender, EventArgs e)
        {
            MessageBox.Show("Disposed of file dialog");
           // throw new NotImplementedException();
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            encryptFileDialog.ShowDialog();
        }

        private void decryptButton_Click(object sender, EventArgs e)
        {
            decryptFileDialog.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

     
        private void decryptFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            saveFileButton.Visible = true;
            saveFileButton.Text = "Save decrypted file";

            translatedLines = File.ReadAllLines(decryptFileDialog.FileName);

            translatedLines = DecryptText(translatedLines);


            saveFileDialog1.FileName = "decrypted";
        }

        private void encryptFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            saveFileButton.Visible = true;
            saveFileButton.Text = "Save encrypted file";

            translatedLines = File.ReadAllLines(encryptFileDialog.FileName);

            translatedLines = EncryptText(translatedLines);

            saveFileDialog1.FileName = "encrypted";
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            Console.WriteLine(saveFileDialog1.FileName);
            saveFileButton.Visible = false;
            // MessageBox.Show("Saved encrypted file to: " + saveFileDialog1.FileName);

            File.WriteAllLines(saveFileDialog1.FileName, translatedLines);
        }

        private void saveFileButton_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private string[] EncryptText(string[] texts)
        {

            for (int i = 0; i < texts.Length; i++)
            {
                StringBuilder builder = new StringBuilder();
                texts[i] = texts[i].ToUpper();

                for (int j = 0; j < texts[i].Length; j++)
                {
                    char letter = texts[i][j];

                    string numbers = Helper.GetLetterCoordinates(letter);

                    Console.WriteLine(numbers);

                    builder.Append(numbers);
                    builder.Append(" ");
                }

                texts[i] = builder.ToString();
            }


            return texts;

        }

        private string[] DecryptText(string[] texts)
        {
            for (int i = 0; i < texts.Length; i++)
            {

                StringBuilder builder = new StringBuilder();
                texts[i] = texts[i].ToUpper();

                int[] coords = new int[2];


                int numbersFound = 0;

                int whiteSpaceCount = 0;

                for (int j = 0; j < texts[i].Length; j++)
                {
                    if (char.IsDigit(texts[i][j]))
                    {
                        whiteSpaceCount = 0;
                        numbersFound++;

                        coords[numbersFound - 1] = int.Parse(texts[i][j].ToString());

                        if (numbersFound == 2)
                        {
                            char letter = GetLetter(coords[0], coords[1]);
                            builder.Append(letter);
                            numbersFound = 0;
                        }
                    }

                    else if (char.IsWhiteSpace(texts[i][j]))
                    {
                        whiteSpaceCount++;

                        if (whiteSpaceCount > 1)
                        {
                            whiteSpaceCount = 0;
                            builder.Append(" ");
                        }
                    }

                   

                }

                texts[i] = builder.ToString();


            }

            return texts;

        }

       
    }
}
